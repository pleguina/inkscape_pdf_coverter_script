import subprocess

# Replace 'input.svg' with the full path to your SVG file
input_svg = '/path/to/input.svg'

# Replace 'output.pdf' with the full path for the output PDF file
output_pdf = '/path/to/output.pdf'

# Inkscape command to export only the drawing area to PDF
inkscape_cmd = [
    'inkscape',
    '--without-gui',
    '--export-pdf=' + output_pdf,
    '--export-area-drawing',  # Export only the drawing area
    '--export-background-opacity=0',  # Transparent background
    '--export-type=pdf',
    input_svg,
]

try:
    # Run Inkscape command
    subprocess.run(inkscape_cmd, check=True)
    print(f'Successfully exported {input_svg} to {output_pdf}')
except subprocess.CalledProcessError as e:
    print(f'Error: {e}')